import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService, UserService } from 'src/app/core';
import { AssetModel } from 'src/app/model/AssetModel';
import * as URLConstant from '../../../app/constant/URLConstant';
import { SitesModel } from 'src/app/model/SitesModel';
import { DefectSeverityModel } from 'src/app/model/DefectSeverity';
import { SeverityModel } from 'src/app/model/SeverityModel';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  userDetails: Object;
  hamburgerStatus = false;
  showSideBar: Boolean = true;
  type: string;
  stacked: any[] = [];
  defectDtl = new DefectSeverityModel();
  defectSev : DefectSeverityModel[];
  assetOvw : AssetModel;
  sites : SitesModel[];
  sitesName : string[];

  selected: string;
  states: string[] = [
    'Alabama',
    'Alaska',
    'Arizona',
    'Arkansas',
    'California',
    'Wisconsin',
    'Wyoming'
  ];


  constructor(
    private userService: UserService,
    private router: Router,
    private apiService: ApiService,
  ) {
    this.userDetails = this.userService.getSession();
    this.defectDtl.severity = new SeverityModel();
   }

  ngOnInit(): void {
    this.assetDetails();
    this.allSite();
    //this.defectSeverity();
    this.allSiteDefect();
  }


  toggleSideBar(){
    this.showSideBar = !this.showSideBar;
  }

  allSiteDefect(){
    this.apiService._get(URLConstant.DEFECT_SEVERITY).subscribe(response => {
      this.defectSev = response.data;
      //console.info(this.defectDtl[0].severity.High)
    })
  }

  getProgressvalue() {
    return this.defectDtl.severity;
  }

  defectSeverity(){
    let types = ['success', 'info', 'warning', 'danger'];
    this.stacked = [];
    let index = Math.floor(Math.random() * 4);
    let value = Math.floor(Math.random() * 27 + 3);
      this.stacked.push({
        value,
        type: types[index],
        label: value + ' %'
      });
  }

  assetDetails(){
    this.apiService._get(URLConstant.OVERVIEW).subscribe(response => {
      this.assetOvw = response.data;
    })
  }

  allSite(){
    this.apiService._get(URLConstant.SITES).subscribe(response => {
      this.sites = response.data;
      
      if(response.data > 0){
        response.data.array.forEach(element => {
          this.sitesName = element.name;
        });
      }
      //console.info(this.sites)
    })
  }


}
