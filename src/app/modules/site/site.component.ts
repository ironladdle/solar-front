import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SitesModel } from 'src/app/model/SitesModel';
import { ApiService } from 'src/app/core';
import * as URLConstant from '../../../app/constant/URLConstant';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-site',
  templateUrl: './site.component.html',
  styleUrls: ['./site.component.css']
})
export class SiteComponent implements OnInit {

  siteAll : SitesModel[];
  modifiedData : any;

  siteHeader: string[] = [];
  searchText : string = '';
     
  constructor(
    private router: Router,
    private toastr: ToastrService,
    private apiService: ApiService,
    public datepipe : DatePipe
  ) { 
    
  }

  ngOnInit(): void {
    this.viewSite();
  }

  createProject(){
    this.router.navigate(['home/newproject']);
  }

  goSite(site){
    this.router.navigate(['home/site/site-detail'], { queryParams: {site_id: site.id } })
  }

  viewSite(){
    this.apiService._get(URLConstant.SITES).subscribe(response => {
      if(response.data != null){
        response.data.forEach(element => {
          let sd = new Date(element.started_at).toISOString();
          element.started_at = this.datepipe.transform(sd,'dd/MM/yyyy');
          element.start_date = sd;

          let up = new Date(element.updated_at).toISOString();
          element.updated_at = this.datepipe.transform(up,'dd/MM/yyyy');
          element.update_date = up;
        });
        this.siteAll = response.data;
        this.modifiedData = this.siteAll;

        for (let qwe of this.siteAll){
          this.siteHeader.push(qwe.name);
        }
        
      }
    })
  }


searchSite(name: string) {  
  this.siteAll;
    if(name == null || name == ''){
      this.apiService._get(URLConstant.SITES).subscribe(response => {
        if(response.data != null){
          response.data.forEach(element => {
            let sd = new Date(element.started_at).toISOString();
            element.started_at = this.datepipe.transform(sd,'dd/MM/yyyy');
            element.start_date = sd;
  
            let up = new Date(element.updated_at).toISOString();
            element.updated_at = this.datepipe.transform(up,'dd/MM/yyyy');
            element.update_date = up;
          });
          this.siteAll = response.data;
        }
      })
      return this.siteAll;
    }else {
      this.siteAll = this.modifiedData.filter(searchSite => searchSite.name == name); 
    } 
    return this.siteAll;
   }    

}
