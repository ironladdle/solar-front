import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SitesModel } from 'src/app/model/SitesModel';
import * as URLConstant from '../../../app/constant/URLConstant';
import { DatePipe } from '@angular/common';
import { ProfileModel } from 'src/app/model/ProfileModel';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-site-detail',
  templateUrl: './site-detail.component.html',
  styleUrls: ['./site-detail.component.css']
})
export class SiteDetailComponent implements OnInit {

  siteId :number;
  siteDtl = new SitesModel();
  profId : number;
  profile_name : string;
  profile : ProfileModel;

  //ir images
  moduleIRShow: Boolean = false;
  anomaliesIRShow : Boolean = false;

  //ortho
  moduleOrthoShow : Boolean = false;
  moduleTabOrthoShow : Boolean = false;

  //form
  irForm: FormGroup;
  orthoForm: FormGroup;
  reportForm: FormGroup;

  constructor(
    private apiService: ApiService, 
    private router:Router,
    private activatedRoute: ActivatedRoute,
    private toastr : ToastrService,
    public datepipe : DatePipe
  ) { 

    this.irForm = new FormGroup({
      irAnomaliesType : new FormControl(null), 
      irSeverityType : new FormControl(null), 
    });

    this.orthoForm = new FormGroup({
      orthoAnomaliesType : new FormControl(null), 
      orthoSeverityType : new FormControl(null), 
    });

    this.reportForm = new FormGroup({
      rptDate : new FormControl(null), 
      rptCoordinat : new FormControl(null), 
      rptPanel : new FormControl(null), 
    });

  }

  ngOnInit(): void {
    this.siteId = this.activatedRoute.queryParams["_value"].site_id;
    this.toastr.info("Site ID : "+this.siteId);
    this.siteDetails();
  }

  siteDetails(){
    this.apiService._get(URLConstant.SITES + '/'+ this.siteId).subscribe(response => {
      if(response.data != null){
          let sd = new Date(response.data.started_at).toISOString();
          response.data.started_at = this.datepipe.transform(sd,'dd/MM/yyyy');
          response.data.start_date = sd;

          let up = new Date(response.data.updated_at).toISOString();
          response.data.updated_at = this.datepipe.transform(up,'dd/MM/yyyy');
          response.data.update_date = up;
        this.siteDtl = response.data;
        //this.profId = this.siteDtl.profile_id;
        //console.info("siteDetail profId " +this.profId);
          this.apiService._get(URLConstant.PROFILES + '/'+ this.siteDtl.profile_id).subscribe(resp => {
            this.profile = resp.data;
            this.profile_name = this.profile.name;
            //console.info("profile "+ this.profile.name);
          })
      }
    })
  }

  toggleIRModule(){
    this.moduleIRShow = true;
  }

  toggleAnnotation(){
    this.anomaliesIRShow = true;
  }

  toggleOrthoModule(){
    this.moduleOrthoShow = true;
  }

  toggleOrthoAnnotation(){
    this.moduleTabOrthoShow = true;
  }


}
