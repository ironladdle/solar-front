export const LOGIN = '/login';
export const PROFILES = '/profiles'
export const NOTIFICATIONS = '/notifications';
export const SITES = '/sites';
export const OVERVIEW = '/overview';
export const DEFECT_SEVERITY = '/siteDefectsSeverity';
export const ORGANIZATIONS = '/organizations';
