import { Component, OnInit } from '@angular/core';
import { UserService, ApiService, SocketService } from 'src/app/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import * as URLConstant from '../../../constant/URLConstant';
import { NotificationModel } from 'src/app/model/NotificationModel';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  // user details
  userDetails: Object;
  activeParams;
  // hamburger hide/show status
  hamburgerStatus = false;

  notify : NotificationModel[];
  notifiyCount : number;

  constructor(
    private userService: UserService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private apiService: ApiService,
    private socketService: SocketService,

  ) {
    // get user details form session
    this.userDetails = this.userService.getSession();
  }

  ngOnInit() {
    this.getNotification();
  }

  getNotification(){
    this.apiService._get(URLConstant.NOTIFICATIONS).subscribe(response => {
      this.notify = response.data;
      this.notifiyCount = this.notify.length;
    })
  }

 
  logout (){
    window.location.href="/auth/login";
  }

  viewSites(){
    console.info("site management");
    this.router.navigate(['home/site']);
  }

  newSite(){
    console.info("site management");
    this.router.navigate(['home/newproject']);
  }

  userMgmnt(){
    this.router.navigate(['home/user-mgmnt']);
  }
  
}
